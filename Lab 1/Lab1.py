#!/usr/bin/env python
# coding: utf-8

# In[1]:


import numpy as np
import matplotlib.pyplot as plt 
import matplotlib as figure
from matplotlib.lines import Line2D
from scipy.integrate import solve_ivp

plt.style.use('seaborn-whitegrid')

# In[9]:


def f1(gamma):
    def rhs(t, X):
        gamma = 0.
        x, y = X
        return [gamma + y, -x**3-x**2+2*x]
    return rhs

def smpl(t,x):
    return [x**4/4+x**3/3-x**2,x]

# In[13]:


def eq_quiver(rhs, limits, N=20):
    xlims, ylims = limits
    xs = np.linspace(xlims[0], xlims[1], N)
    ys = np.linspace(ylims[0], ylims[1], N)
    U = np.zeros((N, N))
    V = np.zeros((N, N))
    for i, y in enumerate(ys):
        for j, x in enumerate(xs):
            vfield = rhs(0.0, [x, y])
            u, v = vfield
            U[i][j] = u/(u**2+v**2)**(1/2)/2
            V[i][j] = v/(u**2+v**2)**(1/2)/2
    return xs, ys, U, V


# In[14]:


def plotonPlane(rhs, limits):
    plt.close()
    xlims, ylims = limits
    plt.xlim(xlims[0], xlims[1])
    plt.ylim(ylims[0], ylims[1])
    xs, ys, U, V = eq_quiver(rhs, limits)
    plt.quiver(xs, ys, U, V, alpha=0.8)
    


# In[35]:
parametrs_closed = [(-2.75, 1.),(1.4,0.),(-2.6,0.)]
parametrs_point = [(-2.0015,0.),(1.0015,0.)]
parametrs_limit = [(-.000707106781186548,.001),(.000707106781186548,.001)]

# In[33]:
plt.xlabel('x', fontsize = 12)
plt.ylabel('v(x)', fontsize = 12)
gamma = 0
rhs = f1(gamma)
plt.figure(figsize=(100,100))
plotonPlane(rhs, [(-3.,3.),(-3.,3.)])
for param in parametrs_closed:
    sol1 = solve_ivp(rhs, [0., 9.], param, method = 'BDF', rtol=1e-12)
    x1, y1 = sol1.y
    plt.plot(x1, y1, 'forestgreen')
for param in parametrs_point:
    sol1 = solve_ivp(rhs, [0., 9.], param, method = 'BDF', rtol=1e-12)
    x1, y1 = sol1.y
    plt.plot(x1, y1, 'blue')
for param in parametrs_limit:
    sol1 = solve_ivp(rhs, [-9., 9.], param, method = 'BDF', rtol=1e-12)
    x1, y1 = sol1.y
    plt.plot(x1, y1, 'gold')
plt.scatter(0,0,marker='x',c="r")
plt.scatter(1,0,marker='o',c="b")
plt.scatter(-2,0,marker='o',c="b")
custom_lines = [Line2D([0], [0], color='forestgreen', lw=4),
                Line2D([0], [0], color='blue', lw=4),
                Line2D([0], [0], color='gold', lw=4),
                Line2D([0], [0], color="r", lw=4)]

plt.legend(custom_lines, ['Замкнутые линии', 'Устойчивые СР', 'Сепаратрисы','Неустойчивые СР'])
plt.show()

# In[21]:
plt.xlabel('x', fontsize = 12)
plt.ylabel('v(x)', fontsize = 12)
for param in parametrs_closed:
    sol1 = solve_ivp(rhs, [0., 9.], param, method = 'BDF', rtol=1e-12)
    x1, y1 = sol1.y
    plt.plot(x1, y1, 'forestgreen')
for param in parametrs_point:
    sol1 = solve_ivp(rhs, [0., 9.], param, method = 'BDF', rtol=1e-12)
    x1, y1 = sol1.y
    plt.plot(x1, y1, 'blue')
for param in parametrs_limit:
    sol1 = solve_ivp(rhs, [-9., 9.], param, method = 'BDF', rtol=1e-12)
    x1, y1 = sol1.y
    plt.plot(x1, y1, 'gold')
plt.scatter(0,0,marker='+',c="r")
plt.scatter(1,0,marker='o',c="b")
plt.scatter(-2,0,marker='o',c="b")
custom_lines = [Line2D([0], [0], color='forestgreen', lw=4),
                Line2D([0], [0], color='blue', lw=4),
                Line2D([0], [0], color='gold', lw=4),
                Line2D([0], [0], color="r", lw=4)]

plt.legend(custom_lines, ['Замкнутые линии', 'Устойчивые СР', 'Сепаратрисы','Неустойчивые СР'])
plt.show()

# In[ ]:
for param in parametrs_closed:
    plt.xlabel('t', fontsize = 12)
    plt.ylabel('x(t)', fontsize = 12)
    sol1 = solve_ivp(rhs, [0., 20.], param, method = 'BDF', rtol=1e-12)
    x1, y1 = sol1.y
    t = sol1.t
    plt.title("For point = {0}".format(param))
    plt.plot(t,x1)
    plt.show()
for param in parametrs_point:
    plt.xlabel('t', fontsize = 12)
    plt.ylabel('x(t)', fontsize = 12)
    plt.xlim((0,5))
    plt.ylim((-3,3))
    sol1 = solve_ivp(rhs, [0., 9.], param, method = 'BDF', rtol=1e-12)
    x1, y1 = sol1.y
    t = sol1.t
    plt.title("For point = {0}".format(param))
    plt.plot(t,x1)
    plt.show()
for param in parametrs_limit:
    plt.xlabel('t', fontsize = 12)
    plt.ylabel('x(t)', fontsize = 12)
    sol1 = solve_ivp(rhs, [3.5, -12.5], param, method = 'BDF', rtol=1e-12)
    x1, y1 = sol1.y
    t = sol1.t
    plt.title("For point = {0}".format(param))
    plt.plot(t,x1)
    plt.show()

#In[ ]:
plt.xlabel('x', fontsize = 12)
plt.ylabel('U(x)', fontsize = 12)
x = np.linspace(-3,2,1000)
y = x**4/4 + x**3/3 - x**2
plt.figure(1)
plt.plot(x,y)
plt.show()
#In[ ]:
x = np.linspace(-3,2,1000)
y = x**4/4 + x**3/3 - x**2
for param in parametrs_closed:
    sol1 = solve_ivp(rhs, [0., 9.], param, method = 'BDF', rtol=1e-12)
    x1, y1 = sol1.y
    plt.plot(x1, y1, 'forestgreen')
for param in parametrs_point:
    sol1 = solve_ivp(rhs, [0., 9.], param, method = 'BDF', rtol=1e-12)
    x1, y1 = sol1.y
    plt.plot(x1, y1, 'blue')
for param in parametrs_limit:
    sol1 = solve_ivp(rhs, [-9., 9.], param, method = 'BDF', rtol=1e-12)
    x1, y1 = sol1.y
    plt.plot(x1, y1, 'gold')
plt.scatter(0,0,marker='x',c="r")
plt.scatter(1,0,marker='o',c="b")
plt.scatter(-2,0,marker='o',c="b")
plt.plot(x,y,"grey", linestyle='--')
custom_lines = [Line2D([0], [0], color='forestgreen', lw=4),
                Line2D([0], [0], color='blue', lw=4),
                Line2D([0], [0], color='gold', lw=4),
                Line2D([0], [0], color="r", lw=4),
                Line2D([0], [0], color="grey", lw=4)]

plt.legend(custom_lines, ['Замкнутые линии', 'Устойчивые СР', 'Сепаратрисы','Неустойчивые СР','U(x)'])
plt.show()

# %%

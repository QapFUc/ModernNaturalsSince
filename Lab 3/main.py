#!/usr/bin/env python
# coding: utf-8

# In[1]:


import numpy as np
import matplotlib.pyplot as plt 
import matplotlib as figure
from matplotlib.lines import Line2D
from scipy.integrate import solve_ivp
import math


# In[9]:


def f1(gamma,betta,delta):
    def rhs1(t, X1, X2):
        a = gamma
        b = betta
        x1, y = X
        return [y, -a*y -x**3-x**2+2*x]
    def rhs2(t, X):
        a = delta
        b = betta
        x, y = X
        return [y, -a*y -x**3-x**2+2*x]
    return rhs1,rhs2

def smpl(t,x):
    return [x**4/4+x**3/3-x**2,x]


# In[13]:


def eq_quiver(rhs, limits, N=20):
    xlims, ylims = limits
    xs = np.linspace(xlims[0], xlims[1], N)
    ys = np.linspace(ylims[0], ylims[1], N)
    U = np.zeros((N, N))
    V = np.zeros((N, N))
    for i, y in enumerate(ys):
        for j, x in enumerate(xs):
            vfield = rhs(0.0, [x, y])
            u, v = vfield
            U[i][j] = u/(u**2+v**2)**(1/2)/2
            V[i][j] = v/(u**2+v**2)**(1/2)/2
    return xs, ys, U, V


# In[14]:
def plotonPlane(rhs, limits):
    plt.close()
    xlims, ylims = limits
    plt.xlim(xlims[0], xlims[1])
    plt.ylim(ylims[0], ylims[1])
    xs, ys, U, V = eq_quiver(rhs, limits)
    plt.quiver(xs, ys, U, V, alpha=0.8)
    
def lamdaPlusPoints(X, Y, alpha):
    res = []
    x = X
    for y in Y:
        res.append(1/2 * (x**2 + y**2 + math.sqrt((y**2 - x**2)**2 + 4 * alpha**2)))
    return res

def lamdaMinusPoints(X, Y, alpha):
    res = []
    x = X
    for y in Y:
        res.append(1/2 * (x**2 + y**2 - math.sqrt((y**2 - x**2)**2 + 4 * alpha**2)))
    return res

def plotLamda(X, Y, alpha):

    lplus = lamdaPlusPoints(X, Y, alpha)
    lminus = lamdaMinusPoints(X, Y, alpha)

    plt.plot(Y, lplus,"r")
    plt.plot(Y, lminus,)
    plt.show()
    # axs[0, 1].plot(X, lminus, 'bo')
def myrange(start,stop,step):
    res = []
    while start < stop:
        res.append(start+step)
        start += step
    return res

def solve_x1(X,Y,alpha,omega1,omega2):
    res =[]

plotLamda(5,myrange(0.5,10,0.1),0.7)

# In[35]:
# parametrs_limit = [(-3.9, 0),(-3,-1),(-2,-3),(-2,-2),(-1.5,3),
#                     (-1,-3),(-1,-2),(-1,3),(-0.5,3),
#                     (0,-3),
#                     (0.3,-2),(1,-2),(2,1),(2.5,1.6),
#                     (0.5,2),(1,3),(3,2)]

# # In[33]:
# plt.xlabel('x', fontsize = 12)
# plt.ylabel('v(x)', fontsize = 12)
# gamma = 6
# rhs = f1(gamma)
# plt.figure(figsize=(100,100))
# plotonPlane(rhs, [(-4.,4.),(-4.,4.)])
# '''for param in parametrs_limit:
#     sol1 = solve_ivp(rhs, [-10., 10.], param, method = 'BDF', rtol=1e-12)
#     x1, y1 = sol1.y
#     plt.plot(x1, y1, 'gold')
# plt.scatter(0,0,marker='x',c="r")
# plt.scatter(1,0,marker='o',c="b")
# plt.scatter(-2,0,marker='o',c="b")
# custom_lines = [Line2D([0], [0], color='forestgreen', lw=4),
#                 Line2D([0], [0], color='blue', lw=4),
#                 Line2D([0], [0], color='gold', lw=4),
#                 Line2D([0], [0], color="r", lw=4)]

# plt.legend(custom_lines, ['Замкнутые линии', 'Устойчивые СР', 'Сепаратрисы','Неустойчивые СР'])'''
# plt.show()

# # In[21]:
# plt.xlabel('x', fontsize = 12)
# plt.ylabel('v(x)', fontsize = 12)
# for param in parametrs_limit:
#     sol1 = solve_ivp(rhs, [-10., 10.], param, method = 'BDF', rtol=1e-12)
#     x1, y1 = sol1.y
#     plt.plot(x1, y1, 'blue')
# for param in [(-0.00075,.001),(0.00075,-0.001)]:
#     sol1 = solve_ivp(rhs, [0., -3.5], param, method = 'BDF', rtol=1e-12)
#     x1, y1 = sol1.y
#     plt.plot(x1, y1, 'gold')
# for param in [(-0.00075,.001),(0.00075,-0.001)]:
#     sol1 = solve_ivp(rhs, [-10., 15.], param, method = 'BDF', rtol=1e-12)
#     x1, y1 = sol1.y
#     plt.plot(x1, y1, 'gold')
# plt.scatter(0,0,marker='+',c="r")
# plt.scatter(1,0,marker='o',c="b")
# plt.scatter(-2,0,marker='o',c="b")
# custom_lines = [Line2D([0], [0], color='forestgreen', lw=4),
#                 Line2D([0], [0], color='blue', lw=4),
#                 Line2D([0], [0], color='gold', lw=4),
#                 Line2D([0], [0], color="r", lw=4)]

# plt.legend(custom_lines, ['Замкнутые линии', 'Устойчивые СР', 'Сепаратрисы','Неустойчивые СР'])
# plt.show()
# for param in parametrs_limit:
#     plt.xlabel('t', fontsize = 12)
#     plt.ylabel('x(t)', fontsize = 12)
#     sol1 = solve_ivp(rhs, [-10, 10], param, method = 'BDF', rtol=1e-12)
#     x1, y1 = sol1.y
#     t = sol1.t
#     plt.title("For point = {0}".format(param))
#     plt.plot(t,x1)
#     plt.show()

# %%

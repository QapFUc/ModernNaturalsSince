#!/usr/bin/env python
# coding: utf-8
import numpy as np
import matplotlib.pyplot as plt 
import matplotlib as figure
from matplotlib.lines import Line2D
from scipy.integrate import solve_ivp
from scipy.integrate import odeint 
import math
from itertools import product

def soe_rhs(vars, t, params):
# """
# 	let x1, x2, y1 = x1', y2 = x2' in vars
	
# 	params =  [a1, a2, a3, a4] something like this  
	
# 	a1 = k1/m1, a2 = k2/m1, a3 = k1/m2, a4 = k2/m2;

# 	t = [.....] = {ti}
# 	"""

	x1, y1, x2, y2 = vars 
	m1,m2,k1,k2,k3 = params

	rhs = [y1,
		   (-k1/m1)*x1 + (k2/m1)*(x2-x1),
		   y2,
		   (-k1/m2)*x2 - (k2/m2)*(x2-x1)]
	return rhs


# ODE calc params 
abserr = 1.e-8
relerr = 1.e-6
stoptime = 160.
numpoints = 5000

def lamdaPlusPoints(X, Y, alpha):
    res = []
    x = X
    for y in Y:
        res.append(1/2 * (x**2 + y**2 + math.sqrt((y**2 - x**2)**2 + 4 * alpha**2)))
    return res

def lamdaMinusPoints(X, Y, alpha):
    res = []
    x = X
    for y in Y:
        res.append(1/2 * (x**2 + y**2 - math.sqrt((y**2 - x**2)**2 + 4 * alpha**2)))
    return res

def plotLamda(X, Y, alpha):

    lplus = lamdaPlusPoints(X, Y, alpha)
    lminus = lamdaMinusPoints(X, Y, alpha)

    plt.plot(Y, lplus,"r")
    plt.plot(Y, lminus,)
    plt.show()
    # axs[0, 1].plot(X, lminus, 'bo')
def myrange(start,stop,step):
    res = []
    while start < stop:
        res.append(start+step)
        start += step
    return res

def solve_x1(X,Y,alpha,omega1,omega2):
    res =[]


t = [stoptime * float(i) / (numpoints - 1) for i in range(numpoints)]

p = [7,6,7,70,6]

plotLamda(5,myrange(0.5,10,0.1),p[3]/(p[0]*p[1]))

w0 = [3., 5., -2., 0.]

# lamda1 = lambda x, y, alpha: 1/2 * (x**2 + y**2 + math.sqrt((y**2 - x**2)**2 + 4 * alpha**2))
# lamda2 = lambda x, y, alpha: 1/2 * (x**2 + y**2 - math.sqrt((y**2 - x**2)**2 + 4 * alpha**2))

# alpha = p[3]/(p[0]*p[1])
# om1s = [float(i) / 50. for i in range(1, 10)]
# om2s = [float(i) / 50. for i in range(1, 10)]
# lambdas= [[lamda1(w, 1.5, alpha) for w in om1s], [lamda2(w, 1.5, alpha) for w in om1s], [lamda1(1.5, w, alpha) for w in om2s], [lamda2(1.5, w, alpha) for w in om2s]]

# def find_freq(lp1, lp2):
#     diff = []
#     for li, lj in product(lp1, lp2):
#         diff.append((li[0], lj[0], abs(li[1] - lj[1])))
#         print(diff)
            
#     min_elem = min(diff, key = lambda x: x[2])
#     return (min_elem[0], min_elem[1])

omega1, omega2 = p[2]/p[0]+p[3]/p[0] , p[2]/p[1]+p[4]/p[1]

freq_diff = abs(omega1 - omega2)

print("freq 1 = {0}, freq 2 = {1}, freq diff = {2}".format(omega1, omega2, freq_diff))

# solution = [wi, forall i from range(numpoints)]
solution = odeint(soe_rhs, w0, t, args=(p,), atol=abserr, rtol=relerr)

X1 = [w[0] for w in solution]
X2 = [w[2] for w in solution]

plt.xlabel('t', fontsize = 12)
plt.ylabel('xi', fontsize = 12)
plt.title("x1(t) & x2(t)")
plt.plot(t, X1, 'b')
plt.plot(t, X2, 'r')

# fig2 = plt.figure(2)

# fig2.add_subplot(1, 2, 1)
# plt.xlabel("w1", fontsize = 12)
# plt.ylabel("lambda1,2", fontsize = 12)
# plt.title("lambda(w1)")

# plt.plot(om1s, lambdas[0], 'b')
# plt.plot(om1s, lambdas[1], 'r')

# fig2.add_subplot(1, 2, 2)
# plt.xlabel("w2", fontsize = 12)
# plt.ylabel("lambda1,2", fontsize = 12)
# plt.title("lambda(w2)")

# plt.plot(om2s, lambdas[2], 'b')
# plt.plot(om2s, lambdas[3], 'r')

plt.show()







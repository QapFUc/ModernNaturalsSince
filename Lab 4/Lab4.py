
import matplotlib.pyplot as plt
import math 

plt.grid()
def generate_params(start=0., stop=0., step=1.):
  res = []
  p = start
  while (p < stop):
    res.append(p)
    p = p + step
  return res

def create_diagramm(rhs, params, start):
  yvals = []
  Cols = []
  for k in params:
    data = []
    for s in start:
      x = s
      for i in range(1, 1200):
        x = rhs(x, k)
        if i > 1150:
          data.append(x)
    yvals.append(data)
  plt.plot(params,yvals,'b.',ms=1)
  plt.show()

def rhs(x, k):
  return k*x*(1-x*x)

def myrange(start,stop,step):
    res = []
    while start < stop:
        res.append(start+step)
        start += step
    return res

def create_lamer_diagramm(rhs,x_0,lamda):
  x=[]
  y=[]
  for i in myrange(0,1.5,0.003):
    x.append(i)
    y.append(rhs(i,lamda))
  ordin=[0]
  absc=[x_0]
  absc.append(x_0)
  ordin.append(rhs(x_0,lamda))
  for step in range(40):
    absc.append(ordin[-1])
    ordin.append(ordin[-1])
    absc.append(absc[-1])
    ordin.append(rhs(absc[-1],lamda))
  plt.plot(x,x)
  plt.plot(x,y,"blue")
  plt.plot(absc,ordin,"red")
  plt.show()


prms = generate_params(0., 9., 0.001)
# create_diagramm(rhs, prms, [1.01, 0.1])
# create_lamer_diagramm(rhs,1.1,0.5)
# create_lamer_diagramm(rhs,1.1,1)
# create_lamer_diagramm(rhs,1.1,1.5)
# create_lamer_diagramm(rhs,0.2,1.5)
# create_lamer_diagramm(rhs,1.1,2.1)
create_lamer_diagramm(rhs,0.5,2.3)

